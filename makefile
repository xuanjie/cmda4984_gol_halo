# size of the play
N:=20

gameOfLife:
	gcc -I. -O3 -o gameOfLife gameOfLife.c -lm 

run:
	./gameOfLife $(N)

clean:
	rm gameOfLife

single:
	nvcc -I. -O3 -o single single_halo.cu -lm 

runSingle:
	./single $(N)

cleanSingle:
	rm single

double:
	nvcc -I. -O3 -o double double_halo.cu -lm 

runDouble:
	./double $(N)

cleanDouble:
	rm double

all:
	gcc -I. -O3 -o gameOfLife gameOfLife.c -lm
	nvcc -I. -O3 -o single single_halo.cu -lm 
	nvcc -I. -O3 -o double double_halo.cu -lm 

runAll:
	@echo "=======seriel C code=======\n"
	./gameOfLife $(N)

	@echo "\n=======CUDA shared Single Halo=======\n"
	./single $(N)

	@echo "\n=======CUDA shared Double Halo=======\n"
	./double $(N)

cleanAll:
	rm gameOfLife
	rm single
	rm double
