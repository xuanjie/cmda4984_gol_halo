#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <cuda.h>

// Double Halo Exchanges

// to compile
// make double

// to run
// make runDouble

// to clean
// make cleanDouble

/* function to convert from (i,j) cell index to linear storage index */
__device__ __host__ int idx(int N, int i, int j){
  int n = i + (N + 4)*j;
  return n;  
}

#define NX 7
#define NY 7

/* function to update Inew from Iold */
__global__ void iterateSharedKernel(int N, float *c_Iold, float *c_Inew){

  // declare two 2D shared arrays for Double Exchanges
  __shared__ float s_A[NY][NX];
  __shared__ float s_newA[NY][NX];

  int tx = threadIdx.x;
  int bx = blockIdx.x;
  int dx = blockDim.x;
  
  int ty = threadIdx.y;
  int by = blockIdx.y;
  int dy = blockDim.y;

  // X, Y size
  int sizeX = tx + bx * (dx - 4);
  int sizeY = ty + by * (dy - 4);

  // global index for current tx, ty
  int theIdx = idx(N, sizeX, sizeY);

  s_A[ty][tx] = c_Iold[theIdx]; // load the block

  // must synchronize all threads in thread-block
  // before we start reading from the shared memory array
  __syncthreads();
    
  if(tx>0 && tx+1<dx && ty>0 && ty+1<dy){

    // update s_A => s_newA
    int n = s_A[ty+1][tx-1] + s_A[ty+1][tx+0] + s_A[ty+1][tx+1] +
            s_A[ty+0][tx-1] +                   s_A[ty+0][tx+1] +
            s_A[ty-1][tx-1] + s_A[ty-1][tx+0] + s_A[ty-1][tx+1];

    int oldState = s_A[ty][tx];
    int newState = (oldState==1) ? ( (n==2)||(n==3) ) : (n==3) ;
    s_newA[ty][tx]= newState;
  }

  __syncthreads();

  if(tx>1 && tx+2<dx && ty>1 && ty+2<dy){

    // update s_newA => c_Inew
    int n = s_newA[ty+1][tx-1] + s_newA[ty+1][tx+0] + s_newA[ty+1][tx+1] +
            s_newA[ty+0][tx-1] +                      s_newA[ty+0][tx+1] +
            s_newA[ty-1][tx-1] + s_newA[ty-1][tx+0] + s_newA[ty-1][tx+1];

    int oldState = s_newA[ty][tx];
    int newState = (oldState==1) ? ( (n==2)||(n==3) ) : (n==3) ;

    c_Inew[theIdx] = newState;
  }
}

/* function to print game board for debugging */
void print_board(int N, float *board){
  printf("\n");
  for(int i=2; i<N+2; i=i+1){
    for(int j=2; j<N+2; j=j+1){
      printf("%d", (int)board[idx(N,i,j)]);
    }
    printf("\n");
  }
  printf("\n");
}

/* function to solve for game board using Game of Life rules */
void solve(int N){

  //time_t t;
  /* Intializes integer random number generator */
  //  srand((unsigned) time(&t));
  srand(123456);

  // allocate arrays on the HOST
  // notice the size of these arrays
  float* h_Iold = (float*) calloc((N+4)*(N+4),sizeof(float));
  float* h_Inew = (float*) calloc((N+4)*(N+4),sizeof(float));

  for(int i=2;i<N+2;i=i+1){
    for(int j=2;j<N+2;j=j+1){
      // set board state randomly to 1 or 0 
      h_Iold[idx(N,i,j)] = rand()%2;
    }
  }
  /* print initial board*/
  printf("initial game board:");
  print_board(N, h_Iold);

  // DEVICE VERSION:
  float *c_Iold;
  float *c_Inew;
  // allocate arrays on the DEVICE
  cudaMalloc(&c_Iold, (N+4)*(N+4)*sizeof(float));
  cudaMalloc(&c_Inew, (N+4)*(N+4)*sizeof(float));
  // copy data from HOST to DEVICE arrays
  cudaMemcpy(c_Iold, h_Iold, (N+4)*(N+4)*sizeof(float), cudaMemcpyHostToDevice);

  /* iterate here */
  int count = 0;   // step counter
  int maxsteps = 1000; // maximum number of steps

  do{
    dim3 B(NX,NY,1);
    dim3 G((N+(NX-4)-1)/(NX-4), (N+(NY-4)-1)/(NY-4), 1);
  
    /* iterate from Iold to Inew */
    iterateSharedKernel <<< G , B >>> (N, c_Iold, c_Inew);
    
    /* iterate from Inew to Iold */
    iterateSharedKernel <<< G , B >>> (N, c_Inew, c_Iold);

    // copy data from DEVICE to HOST arrays
    cudaMemcpy(h_Iold, c_Iold, (N+4)*(N+4)*sizeof(float), cudaMemcpyDeviceToHost);

    /* update counter */
    count = count + 1;

    // copy data from DEVICE to h_Inew HOST arrays before check in while loop
    cudaMemcpy(h_Inew, c_Inew, (N+4)*(N+4)*sizeof(float), cudaMemcpyDeviceToHost);

    if (count == 10){
      printf("10 game board1:");
      print_board(N, h_Inew);
    }

    if (count == 10){
      printf("10 game board2:");
      print_board(N, h_Iold);
    }

  }while(memcmp(h_Inew, h_Iold, (N+4)*(N+4)*sizeof(int))!=0 && count <= maxsteps);

  /* print out the cell existence in the whole board, then in cell (1 1) and (10 10)*/
  printf("final game board:");
  print_board(N, h_Iold);
  printf("I_{1 1} = %d\n",   (int)h_Iold[idx(N,1,1)]);
  printf("I_{10 10} = %d\n", (int)h_Iold[idx(N,10,10)]);
  printf("Took %d steps\n", count);

  free(h_Iold);
  cudaFree(c_Inew);
  cudaFree(c_Iold);
}



/* usage: ./main 100 
         to iterate, solve and display the game board of size N*/
int main(int argc, char **argv){

  // to select my assigned GPU
  cudaSetDevice(1);

  /* start timer */
  clock_t begin = clock();
  /* read N from the command line arguments */
  int N = atoi(argv[1]);

  /* to solve for cell existence in game of life game board */
  solve(N);

  /* end timer*/
  clock_t end = clock();
  double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("Time spent = %g sec\n", time_spent);
  return 0;
}
